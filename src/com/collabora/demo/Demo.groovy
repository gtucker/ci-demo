/*
  Copyright (C) 2019 Collabora Limited
  Author: Guillaume Tucker <guillaume.tucker@collabora.com>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

package com.collabora.demo;

def getDockerImage(job_dir, name, path) {
    def docker_image = null

    dir(job_dir) {
        try {
            docker_image = docker.build(name, path)
        } catch (Exception e) {
            /* Make it possible to run without an internet connection, using
             * any image already in the cache if it has been built before. */
            print("Warning: Docker image build failed: ${e}")
            docker_image = docker.image(name)
        }
    }

    return docker_image
}

def cloneKernel(path, url, branch, revision) {
    dir(path) {
        sh(script: """
if [ -d linux ]; then
  cd linux
  git remote set-url origin ${url}
  git remote update origin
else
  git clone ${url} -o origin linux
  cd linux
fi
git reset --hard
git fetch origin ${branch}
git checkout ${revision}
""")
    }
}

def parseResults(job_dir, callback) {
    def json_file = "${env.WORKSPACE}/callback.json"
    writeFile(file: json_file, text: callback)
    def result = null

    dir(job_dir) {
        result = sh(returnStatus: true, script: """
python3 lava/result.py ${json_file}
""")
    }

    sh(script: "rm -f ${json_file}")

    return result
}
