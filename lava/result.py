#  Copyright (C) 2019 Collabora Limited
#  Author: Guillaume Tucker <guillaume.tucker@collabora.com>
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to
#  deal in the Software without restriction, including without limitation the
#  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
#  sell copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in
#  all copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
#  IN THE SOFTWARE.

import argparse
import json
import sys
import yaml

# copied from lava-server/lava_scheduler_app/models.py
SUBMITTED = 0
RUNNING = 1
COMPLETE = 2
INCOMPLETE = 3
CANCELED = 4
CANCELING = 5

LAVA_JOB_RESULT_NAMES = {
    COMPLETE: "PASS",
    INCOMPLETE: "FAIL",
    CANCELED: "UNKNOWN",
    CANCELING: "UNKNOWN",
}


def parse_test_results(data):
    all_pass = True
    results = data['results']
    for name, test_results_yaml in results.items():
        if name == 'lava':
            continue
        name = name.split('_')[1]
        print("Test: {}".format(name))
        test_results = yaml.load(test_results_yaml)
        for test_case in test_results:
            name, result = (test_case[x] for x in ['name', 'result'])
            print("* {}: {}".format(name, result))
            all_pass = all_pass and result == 'pass'
    return all_pass


def main(args):
    with open(args.json) as json_file:
        res = json.load(json_file)
    job_status = res['status']
    print("Job status: {}".format(LAVA_JOB_RESULT_NAMES[job_status]))
    job_passed = bool(job_status == COMPLETE)
    if job_passed:
        tests_passed = parse_test_results(res)
        if tests_passed:
            print("All tests passed")
        else:
            print("Some tests failed")
        job_passed = job_passed and tests_passed
    return job_passed


if __name__ == '__main__':
    parser = argparse.ArgumentParser("LAVA job result parser")
    parser.add_argument("json",
                        help="Path to the JSON file with the results")
    args = parser.parse_args()
    status = main(args)
    sys.exit(0 if status is True else 1)
